import pygame
import pytmx
import pyscroll
from joueur import *
from joueur_2 import *

class Jeu:
    def __init__(self):
        #créer la fenetre du jeu
        self.screen = pygame.display.set_mode((800,600))
        pygame.display.set_caption("OP")

        #charger la carte(tmx)
        tmx_data = pytmx.util_pygame.load_pygame('tkt.tmx')
        map_data = pyscroll.data.TiledMapData(tmx_data)
        map_layer = pyscroll.orthographic.BufferedRenderer(map_data, self.screen.get_size())
        map_layer.zoom = 2


        # génerer un joueur
        joueur_position = tmx_data.get_object_by_name("joueur")
        self.joueur = perso(joueur_position.x, joueur_position.y)

        # génerer un joueur
        joueur_position = tmx_data.get_object_by_name("joueur2")
        self.joueur = perso(joueur_position.x, joueur_position.y)


        # definir une liste qui va stocker les rectangles de collision
        self.walls = []

        for obj in tmx_data.objects:
            if obj.type == "collision":
                self.walls.append(pygame.Rect(obj.x, obj.y, obj.width, obj.height))

        #dessiner le groupe de calque
        self.group = pyscroll.PyscrollGroup(map_layer=map_layer, default_layer=5)
        self.group.add(self.joueur)

        #définir le rect de collision pour entrer dans la maison

        enter = tmx_data.get_object_by_name('entrée1')
        self.enter_rect = pygame.Rect(enter.x, enter.y, enter.width, enter.height)


    def handle_input(self):
        pressed = pygame.key.get_pressed()


        if pressed[pygame.K_UP]:
            self.joueur.move_up()
            self.joueur.change_animation('up')

        elif pressed[pygame.K_DOWN]:
            self.joueur.move_down()
            self.joueur.change_animation('down')

        elif pressed[pygame.K_LEFT]:
            self.joueur.move_left()
            self.joueur.change_animation('left')

        elif pressed[pygame.K_RIGHT]:
            self.joueur.move_right()
            self.joueur.change_animation('right')


    def switch_house(self):
        # charger la carte(tmx)
        tmx_data = pytmx.util_pygame.load_pygame("l_intérieur.tmx")
        map_data = pyscroll.data.TiledMapData(tmx_data)
        map_layer = pyscroll.orthographic.BufferedRenderer(map_data, self.screen.get_size())
        map_layer.zoom = 2

        # definir une liste qui va stocker les rectangles de collision
        self.walls = []

        for obj in tmx_data.objects:
            if obj.type == "collision":
                self.walls.append(pygame.Rect(obj.x, obj.y, obj.width, obj.height))

        # dessiner le groupe de calque
        self.group = pyscroll.PyscrollGroup(map_layer=map_layer, default_layer=5)
        self.group.add(self.joueur)

        # définir le rect de collision pour sortir de la maison
        enter = tmx_data.get_object_by_name('sortie_maison')
        self.enter_rect = pygame.Rect(enter.x, enter.y, enter.width, enter.height)

        # recuperer le point de spawn dans la maison
        spwan_house_point = tmx_data.get_object_by_name('spawn-maison')
        self.joueur.position[0] = spwan_house_point.x
        self.joueur.position[1] = spwan_house_point.y - 20

    def switch_map(self):
        # charger la carte(tmx)
        tmx_data = pytmx.util_pygame.load_pygame("tkt.tmx")
        map_data = pyscroll.data.TiledMapData(tmx_data)
        map_layer = pyscroll.orthographic.BufferedRenderer(map_data, self.screen.get_size())
        map_layer.zoom = 2

        # definir une liste qui va stocker les rectangles de collision
        self.walls = []

        for obj in tmx_data.objects:
            if obj.type == "collision":
                self.walls.append(pygame.Rect(obj.x, obj.y, obj.width, obj.height))

        # dessiner le groupe de calque
        self.group = pyscroll.PyscrollGroup(map_layer=map_layer, default_layer=5)
        self.group.add(self.joueur)

        # définir le rect de collision pour entrer  de la maison
        enter_house = tmx_data.get_object_by_name('entrée1')
    
        self.enter_rect = pygame.Rect(enter_house.x, enter_house.y, enter_house.width, enter_house.height)

        #recuperer le point de spawn devant la maison
        spwan_map_point= tmx_data.get_object_by_name('sortie1')
        self.joueur.position[0] = spwan_map_point.x
        self.joueur.position[1] = spwan_map_point.y - 50



    def update(self):
        self.group.update()

        # verifier l'entrer dans la maison
        if self.joueur.feet.colliderect(self.enter_rect):
            self.switch_house()


        # verifier l'entrer dans la maison
        if self.joueur.feet.colliderect(self.enter_rect):
            self.switch_map()


        #verification collision
        for sprite in self.group.sprites():
            if sprite.feet.collidelist(self.walls) > -1:
                sprite.move_back()

    def run(self):

        clock = pygame.time.Clock()

        #boucle du jeu
        boucle = True

        while boucle:

            self.joueur.save_location()
            self.handle_input()
            self.update()
            self.group.center(self.joueur.rect.center)
            self.group.draw(self.screen)
            pygame.display.flip()

            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    boucle = False
            clock.tick(60)
        pygame.quit()